<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property string $id
 * @property string $key
 * @property string $value
 * @property integer $is_file
 */
class Config extends CActiveRecord
{
	public $file;
	
	public static $params = array();
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('id, key, is_file', 'required'),
			
			array('value', 'required', 'on' => 'string_param'),
			
			array('is_file', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>16),
			array('key, value', 'length', 'max'=>255),
			
			array('value, file', 'safe'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, key, value, is_file', 'safe', 'on'=>'search'),
		);
		
		if ($this->isNewRecord)
		{
			$rules[] = array('file', 'required', 'on' => 'file_param');
		}
		
		return $rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$labels = array(
			'id' => 'ID',
			'key' => 'Название',
			'value' => 'Значение',
			'is_file' => 'Файл',
			'file' => 'Файл',
		);
		
		if ($this->id === 'password')
		{
			$labels['key'] = 'Логин';
			$labels['value'] = 'Пароль';
		}	
		
		return $labels;
	}
	
	public function behaviors()
	{
		return array(
			array(
				'class' => 'UploadFileBehavior',
				'attributeNames' => array(
					'file' => array(
						'types' => array('any_file'),
						'image' => false,
					),
				),
				'saveOriginalFilename' => true,
				'fileTypes' => array(
					'any_file' => 'pdf, doc, docx, txt, xls, xlsx, rtf, jpeg, jpg, png, rar, zip',
				),	
			),
		);
	}
	
	public function defaultScope()
	{
		return array(
			'condition' => 't.is_hidden = 0',
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('is_file',$this->is_file);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function hasAttribute($name)
	{
		if (!parent::hasAttribute($name))
		{
			return property_exists($this, $name);
		}
		else
			return true;
	}
	
	protected function beforeSave()
	{
		$result = false;
		
		if (parent::beforeSave())
		{
			if ($this->scenario == 'file_param' && !empty($this->file))
			{
				$this->value = $this->file;
			}
			
			if ($this->id == 'password')
			{
				$salt = $this->randomSalt();
				$this->value = sha1($this->value . $salt);
				
				$saltModel = self::model()->resetScope()->findByPk('salt');
				
				if ($saltModel === null)
				{
					$saltModel = new Config('string_param');
					$saltModel->attributes = array(
						'id' => 'salt',
						'key' => 'salt',
						'is_hidden' => 1,
						'is_file' => 0,
					);
				}
				
				$saltModel->value = $salt;
				
				$result = $saltModel->save();
			}
			else
				$result = true;
		}
		
		return $result;
	}
	
    public function randomSalt($length = 8)
    {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime() * 1000000);
        $i = 1;
        $salt = '' ;

        while ($i <= $length)
        {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $salt .= $tmp;
            $i++;
        }
		
        return $salt;
    } 
	
	public static function getParamAttr($param, $attribute)
	{
		if (!isset(self::$params[$param]))
		{
			$model = self::model()->resetScope()->findByPk($param);
			
			if ($model !== null)
			{
				self::$params[$param] = $model;
				$result = $model->$attribute;
			}
			else
				$result = '';
		}
		else
			$result = self::$params[$param]->$attribute;
			
		return $result;
	}
	
	public static function key($param)
	{
		return self::getParamAttr($param, 'key');
	}
	
	public static function value($param)
	{
		return self::getParamAttr($param, 'value');
	}
}
