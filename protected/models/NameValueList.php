<?php

/**
 * This is the model class for table "namevaluelist".
 *
 * The followings are the available columns in table 'namevaluelist':
 * @property integer $id
 * @property string $name
 * @property integer $position
 * @property string $image
 * @property string $price
 * @property string $description
 */
class NameValueList extends MMActiveRecord
{
	public $params = array();

	/**
	 * @return string the associated database table name
	 */

	public function tableName()
	{
		return 'namevaluelist';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, name', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => false,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
	{
		return array(
			array(
				'class' => 'JSONParamsBehavior',
				'listAttribute' => 'params',
				'jsonAttribute' => 'content',
				'params' => array(
					'name', 'value',
				),
			),
		);
	}

	public static function get($id)
	{
		$model = self::model()->findByPk($id);
		if ($model)
		{
			return $model->params;
		}	
		else
			return array();
	}
}
