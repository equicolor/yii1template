<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $role
 */
class User extends MMActiveRecord
{
	const ROLE_ADMIN = 'admin';
	const ROLE_MODERATOR = 'moderator';

	private $_oldPassword;
	
	public static function getRoles()
	{
		return array(
			self::ROLE_ADMIN => 'Админ',
			self::ROLE_MODERATOR => 'Модератор',
		);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, role', 'required'),
			array('username', 'length', 'max'=>40),
			array('salt', 'length', 'max'=>16),
			array('username', 'unique'),
			array('password', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, role', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Логин',
			'password' => 'Пароль',
			'salt' => 'Соль',
			'role' => 'Роль',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('role',$this->role,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getRoleName()
	{
		$roles = self::getRoles();
		return isset($roles[$this->role]) ? $roles[$this->role] : $this->role;
	}

	public function getIsAdmin()
	{
		return $this->role == self::ROLE_ADMIN;
	}

	public function getIsModerator()
	{
		return $this->role == self::ROLE_MODERATOR;
	}

	public static function generateString($length = 16) 
	{
		return substr(md5(uniqid(time())), 0, $length);
	}

	public static function hashPassword($password, $salt) 
	{
		return sha1($password . $salt);
	}
	
	public function generatePassword()
	{
		$this->salt = self::generateString();
		$this->password = self::hashPassword($this->password, $this->salt);
	}
	
	public function getIsPasswordChanged()
	{
		return !empty($this->password) && $this->_oldPassword !== $this->password;
	}
	
	protected function afterFind()
	{
		parent::afterFind();
		
		$this->_oldPassword = $this->password;
	}
	
	protected function beforeValidate()
	{
		if (parent::beforeValidate())
		{
			if ($this->isNewRecord)
			{
				$validator = new CRequiredValidator;
				$validator->attributes = array('password');
				$this->validatorList->add($validator);
			}
			
			return true;
		}
		else
			return false;
	}

	protected function beforeSave()
	{
		if (parent::beforeSave())
		{
			if ($this->isPasswordChanged)
			{
				$this->generatePassword();
			}
			else
				$this->password = $this->_oldPassword;

			return true;
		}
		else
			return false;
	}
}
