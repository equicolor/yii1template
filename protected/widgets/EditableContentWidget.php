<?php

class EditableContentWidget extends CWidget 
{
	public $content;
	public $id;
	public $hideEditLink = false;
	public $stripTags = false;

	public function run() 
	{
		if (Yii::app()->user->isGuest)
		{
			echo $this->content; //strip_tags(str_replace('<br>', ' ', $this->content));
		}
		else
		{
			$this->render('editableContent', array(
				'content' => $this->content,
				'id' => $this->id,
			));
		}
	}
}