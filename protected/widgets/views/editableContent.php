<?php if (!$this->hideEditLink): ?>
	<?= CHtml::link('Править', array('admin/content/update', 'id' => $id), array(
		'target' => '_blank',
		'class' => 'edit-link',
	)); ?>
<?php endif; ?>
<?php 
	if ($this->stripTags)
	{
		$content = strip_tags(str_replace('<br>', ' ', $content));
	}
?>

<?= $content;?>