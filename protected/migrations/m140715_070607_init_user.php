<?php

class m140715_070607_init_user extends CDbMigration
{
	public function up()
	{
		$this->createTable('user', array(
			'id' => 'pk',
			'username' => 'varchar(40) NOT NULL UNIQUE',
			'password' => 'varchar(40) NOT NULL',
			'salt' => 'varchar(16) NOT NULL',
			'role' => 'enum("admin", "moderator") DEFAULT "admin" NOT NULL',
		)); 
	}

	public function down()
	{
		$this->dropTable('user');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}