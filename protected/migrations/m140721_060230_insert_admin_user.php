<?php

class m140721_060230_insert_admin_user extends CDbMigration
{
	public function up()
	{
		$this->insert('user', array(
			'username' => 'admin',
			'password' => 'ce4672c5318a752abcc28d20781857b30354feee',
			'salt' => 'd90f521c1f7fef8f',
			'role' => 'admin',
		));
	}

	public function down()
	{
		$this->delete('user', 'username = :username', array(':username' => 'admin'));
	}
}