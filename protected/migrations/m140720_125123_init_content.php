<?php

class m140720_125123_init_content extends CDbMigration
{
	public function up()
	{
		$this->createTable('content', array(
			'id' => 'varchar(255) NOT NULL PRIMARY KEY',
			'content' => 'text NOT NULL',
			'name' => 'string NOT NULL',
		));
	}

	public function down()
	{	
		$this->dropTable('content');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}