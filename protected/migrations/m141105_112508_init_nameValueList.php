<?php

class m141105_112508_init_nameValueList extends CDbMigration
{
	public function up()
	{
		$this->createTable('nameValueList', array(
			'id' => 'string NOT NULL PRIMARY KEY',
			'name' => 'string NOT NULL',
			'content' => 'text NOT NULL',
		));
	}

	public function down()
	{
		$this->dropTable('nameValueList');
	}


	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}