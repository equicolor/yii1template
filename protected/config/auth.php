<?php

return array(
	'moderator' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'moderator',
		'children' => array('reader'),
		'bizRule' => null,
		'data' => null
	),
	'admin' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Администратор',
		'children' => array('moderator'),
		'bizRule' => null,
		'data' => null
	),
);