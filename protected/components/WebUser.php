<?php

class WebUser extends CWebUser 
{
	// private $_role = 'guest';

	private $_model;

    function getRole() 
    {
		return $this->model->role;
    }
	
	public function getModel()
	{
		if ($this->_model === null)
		{
			$this->_model = User::model()->findByPk($this->id);
		}

		return $this->_model;
	}
}