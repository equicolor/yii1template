<?php
class MMActiveRecord extends CActiveRecord
{
	public function limit($limit)
	{
		$this->dbCriteria->limit = $limit;

		return $this;
	}

	public function order($order)
	{
		$this->dbCriteria->mergeWith(array(
			'order' => $order,
		));

		return $this;
	}

	public function simpleConditionScope($attribute, $value, $operator = '=')
	{
		$criteria = $this->dbCriteria;
		$t = $this->getTableAlias();
		$criteria->addCondition("$t.$attribute $operator :$attribute");
		$criteria->params[':' . $attribute] = $value;

		return $this;
	}	
}