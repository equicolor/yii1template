<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Настройки'=>array('admin'),
	'Список',
);

$this->menu=array(
	// array('label'=>'List Config', 'url'=>array('index')),
	array('label'=>'Создать параметр', 'url'=>array('create', 'file' => 0)),
	array('label'=>'Создать файл', 'url'=>array('create', 'file' => 1)),
);

$this->pageTitle = 'Список настроек';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'config-grid',
	'type' => TbHtml::GRID_TYPE_STRIPED,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'key',
		'value',
		array(
			'header' => 'Тип',
			'value' => '$data->is_file ? "файл" : "текст"',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>
