<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Настройки'=>array('admin'),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать параметр', 'url'=>array('create', 'file' => 0)),
	array('label'=>'Создать файл', 'url'=>array('create', 'file' => 1)),
	array('label'=>'Список настроек', 'url'=>array('admin')),
);

$this->pageTitle = 'Редактирование параметра ' . $model->id;
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>