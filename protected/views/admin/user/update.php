<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('admin')),
	array('label'=>'Добавить', 'url'=>array('create')),
);

$this->pageTitle = 'Редактирование пользователя ' . $model->username;
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>