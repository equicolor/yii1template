<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('admin')),
);

$this->pageTitle = 'Создать пользователя';
?>

<?php $this->renderPartial('_form', array(
	'model' => $model,
)); ?>