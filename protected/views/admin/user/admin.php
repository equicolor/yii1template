<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
);

$this->pageTitle = 'Список пользователей';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'day-grid',
	'type' => TbHtml::GRID_TYPE_STRIPED,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',	
		'username',
		array(
			'name' => 'role',
			'value' => '$data->roleName',
			'filter' => CHtml::dropDownList('User[role]', $model->role,
			array(
				'admin' => 'Админ',
				'moderator' => 'Модератор',
			), array('empty' => 'Все')),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>
