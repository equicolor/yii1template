<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Списки'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Добавить', 'url'=>array('create')),
);

$this->pageTitle = 'Список списков';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'advantage-grid',
	'type' => TbHtml::GRID_TYPE_STRIPED,
	'dataProvider'=>$model->search(),
	'filter'=>$model,	
	'columns'=>array(
		'id',	
		'name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>