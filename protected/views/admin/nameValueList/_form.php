<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */
?>
<?php Yii::import('ext.imperavi-redactor-widget.ImperaviRedactorWidget'); ?>

<div class="form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'config-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?= $form->textFieldControlGroup($model, 'id', array('span' => 5, 'maxlength' => 255)); ?>
	<?= $form->textFieldControlGroup($model, 'name', array('span' => 5, 'maxlength' => 255)); ?>

	<label>Список название-значение</label>
	<div id="solutionParams" class="grid-view">
		<table class="items table table-condensed table-striped">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($model->params as $key => $param): ?>
				<tr class="filters">
					<td>
						<div class="filter-container">
							<input class="span3" type="text" value="<?= @$param['name']; ?>" name="NameValueList[params][<?= $key ?>][name]">
						</div>								
					</td>
					<td>
						<div class="filter-container">
							<input class="span3" type="text" value="<?= @$param['value']; ?>" name="NameValueList[params][<?= $key ?>][value]">
						</div>								
					</td>
					<td>
						<?= TbHtml::button('Удалить', array('class' => 'solutionParams-delete')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>	
		<?= TbHtml::button('Добавить', array('id' => 'solutionParams-add')); ?>
	</div>				

	<div class="form-actions">
		
		<?php if ($model->isNewRecord): ?>
		
		<?=	TbHtml::submitButton('Создать', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
			'name' => 'apply',
		)); ?>
		
		<?php else: ?>
		
		<?=	TbHtml::submitButton('Сохранить', array(
			'color' => TbHtml::BUTTON_COLOR_PRIMARY,
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
		)); ?>
		
		<?=	TbHtml::submitButton('Применить', array(
			'size' => TbHtml::BUTTON_SIZE_DEFAULT,
			'name' => 'apply',
		)); ?>
		
		<?php endif; ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
	$('#solutionParams-add').click(function() {
		$row = $('#solutionParams tbody tr:last').clone()
		$row.find('input').each(function () {
			name = $(this).attr('name');
			index = /\d+/.exec(name)[0];
			index++;
			$(this).attr('name', $(this).attr('name').replace(/\d+/, index));
			$(this).attr('value', '');
		});
		$row.insertAfter('#solutionParams tbody tr:last');
		toggleSolutionParamsDeleteButton();
	});
	

	$('#solutionParams').on('click', 'button.solutionParams-delete', function(event) {
		$(this).parents('tr').remove();
		toggleSolutionParamsDeleteButton();
	});
	
	function toggleSolutionParamsDeleteButton()
	{
		if ($('#solutionParams tbody tr').length < 2)
		{
			$('button.solutionParams-delete').hide();
		}			
		else
			$('button.solutionParams-delete').show();

	}
</script>