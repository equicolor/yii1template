<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Списки'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('admin')),
);

$this->pageTitle = 'Создать список';
?>

<?php $this->renderPartial('_form', array(
	'model' => $model,
)); ?>