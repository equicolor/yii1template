<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Списки'=>array('admin'),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список', 'url'=>array('admin')),
	array('label'=>'Добавить', 'url'=>array('create')),
);

$this->pageTitle = 'Редактирование списка ' . $model->name;
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>