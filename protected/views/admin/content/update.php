<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Контент'=>array('admin'),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список блоков', 'url'=>array('admin')),
	array('label'=>'Добавить блок', 'url'=>array('create')),
);

$this->pageTitle = 'Редактирование блока ' . $model->id;
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>