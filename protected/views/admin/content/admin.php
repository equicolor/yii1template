<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Контент'=>array('admin'),
	'Список блоков',
);

$this->menu=array(
	array('label'=>'Добавить блок контента', 'url'=>array('create')),
);

$this->pageTitle = 'Список блоков';
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'day-grid',
	'type' => TbHtml::GRID_TYPE_STRIPED,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',	
		'name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>
