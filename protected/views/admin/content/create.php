<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Контент'=>array('admin'),
	'Создать блок контента',
);

$this->menu=array(
	array('label'=>'Список блоков', 'url'=>array('admin')),
);

$this->pageTitle = 'Создать блок контента';
?>

<?php $this->renderPartial('_form', array(
	'model' => $model,
)); ?>