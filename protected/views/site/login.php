<?
/**
 * @var TbActiveForm $form
 * @var LoginForm $model
 */
?>
<div class="container">

	<?php if (Yii::app()->user->hasFlash('password')): ?>
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?= Yii::app()->user->getFlash('password'); ?>
		</div>
	<?php endif; ?>
	
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'hideInlineErrors' => true,
		'htmlOptions' => array(
			'class' => 'form-signin',
		),
	)); ?>
	<h2 class="form-signin-heading">Вход</h2>
	<?= $form->errorSummary($model, 'Возникли следующие ошибки:') ?>
	<?=
	$form->textFieldControlGroup($model, 'username', array(
		'class' => 'input-block-level',
		'label' => false,
		'placeholder' => $model->getAttributeLabel('username')
	))?>
	<?=
	$form->passwordFieldControlGroup($model, 'password', array(
		'class' => 'input-block-level',
		'label' => false,
		'placeholder' => $model->getAttributeLabel('password')
	))?>

	<button class="btn btn-large btn-primary" type="submit">Войти</button>
	<!--
	<a href="<?= $this->createUrl('admin/site/passwordRecovery'); ?>" class="btn btn-large" type="submit">Я забыл пароль</a>
	-->
	<?php $this->endWidget(); ?>

</div> <!-- /container -->