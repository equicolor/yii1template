<?php /** @var AdminController $this * */ ?>
<!DOCTYPE html>
<html>
<head>
	<title><?= $this->pageTitle ?></title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<base href="<?= Yii::app()->getBaseUrl(true); ?>/" />

	<?php Yii::app()->bootstrap->register() ?>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<style type="text/css">
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}

		.sidebar-nav {
			padding: 9px 0;
		}

		@media (max-width: 980px) {
			.navbar-text.pull-right {
				float: none;
				padding-left: 5px;
				padding-right: 5px;
			}
		}
	</style>
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="brand" href="<?= Yii::app()->createUrl('site/index') ?>"><?= Yii::app()->name ?></a>

			<div class="nav-collapse collapse">
				<p class="navbar-text pull-right">
					Вы вошли как <b><?= Yii::app()->user->name ?></b> -
					<a href="<?= Yii::app()->createUrl('site/logout') ?>" class="navbar-link">Выйти</a>
				</p>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
				'links' => $this->breadcrumbs,
			))?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span2 well sidebar-nav">
			<?php $this->widget('bootstrap.widgets.TbNav', array(
				'type' => TbHtml::NAV_TYPE_LIST,
				'items' => array(
					array('label' => 'Главная'),
					array('label' => 'Блоки контента', 'url' => array('admin/content/admin')),
					TbHtml::menuDivider(),
					array('label' => 'Настройки'),
					array('label' => 'Настройки', 'url' => array('admin/config/admin')),
					array('label' => 'Пользователи', 'url' => array('admin/user/admin')),
					TbHtml::menuDivider(),
					array('label' => 'На сайт', 'url' => array('site/index')),
				),
			))?>
		</div>
		<!--/span-->

		<?= $content ?>
	</div>
</div>

<footer class="text-center">
	<hr>
	<p>&copy;
		<a href="http://medvedevmarketing.ru/" target="_blank">Медведев Маркетинг</a>
			2014<?= 2014 == ($date = date('Y')) ? '' : '-' . $date ?>
	</p>
</footer>

</body>
</html>