<!DOCTYPE html>
<html>
	<head>
		<base href=".">
		<meta charset="utf-8">
		<title><?= Yii::app()->name; ?></title>
	</head>

	<body>
		<?= $content; ?>
	</body>
</html>