<h3>Новая заявка на сайте <?= Yii::app()->name; ?></h3>
<p>Имя: <?php echo CHtml::encode($name); ?></p>
<p>E-mail: <?php echo CHtml::link(CHtml::encode($email), 'mailto:'.CHtml::encode($email)); ?></p>
<p>Телефон: <?= CHtml::encode($phone); ?></p>